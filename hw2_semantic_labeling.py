# !pip install torchmetrics
# !pip install transformers
# !pip install torchtext
import os
import time
import random
import zipfile
from torch._C import device
from tqdm import *
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from torchmetrics import *
from torchtext.legacy import data
from torchtext.legacy import datasets

from transformers import BertTokenizer, BertModel
from transformers.file_utils import MODEL_CARD_NAME

# os.environ["CUDA_VISIBLE_DEVICES"]=""

BEST_MODEL_CHECKPOINT = 'best_model_chkpt.pt'

class OAPTaggerDataset(datasets.SequenceTaggingDataset):
    # urls = [('https://miniodis-rproxy.lisn.upsaclay.fr/py3-private/dataset_data_file/870fb887-23c0-43a1-9673-82e7faa4d52f/dataset_stage2.zip?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=EASNOMJFX9QFW4QIY4SL%2F20211210%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20211210T184742Z&X-Amz-Expires=86400&X-Amz-SignedHeaders=host&X-Amz-Signature=cc0e84247490716a8227e1b57856ad0bba0f9fd072ab231590ddcfe5e2e056fd','dataset_stage2.zip')]
    urls = [('https://filetransfer.io/data-package/Z5yiRaPl/download','dataset_stage2.zip')]
    dirname = ''
    name = 'SRL_comparative'


def get_fields(tokenizer):
    init_token_idx = tokenizer.convert_tokens_to_ids(tokenizer.cls_token)
    pad_token_idx = tokenizer.convert_tokens_to_ids(tokenizer.pad_token)
    unk_token_idx = tokenizer.convert_tokens_to_ids(tokenizer.unk_token)
    # print(init_token, pad_token, unk_token)
    # print(init_token_idx, pad_token_idx, unk_token_idx)
    max_inp_len = tokenizer.max_model_input_sizes['bert-base-uncased']
    # print(max_inp_len)
    text_preprocessor = lambda tokens: tokenizer.convert_tokens_to_ids(tokens[:max_inp_len-1])
    tag_preprocessor = lambda tokens: tokens[:max_inp_len-1]

    text_field = data.Field(use_vocab = False,
                            lower = True,
                            preprocessing = text_preprocessor,
                            init_token = init_token_idx,
                            pad_token = pad_token_idx,
                            unk_token = unk_token_idx)

    tag_field = data.Field(unk_token = None,
                           init_token = '<pad>', # to match [CLS] token in the beginning of the text
                           pad_token = '<pad>',
                           preprocessing = tag_preprocessor)

    return  text_field, tag_field

def examples_src_text(f_name):
    res = []
    example = []
    with open(f_name, "r") as f:
        for line in f:
            if line=='\n' or line=='\r\n':
                res.append(example)
                example = []
            else:
                example.append( line.split()[0] )
    if len(example)>0 : res.append(example)
    return res

def get_data(text_field, tag_field):
    ROOT       = '.data'
    TRAIN_FILE = 'train.tsv'
    VAL_FILE   = 'dev.tsv'
    TEST_FILE  = 'test_no_answers.tsv'

    fields = (("text", text_field), ("tags", tag_field))

    train_data, val_data, test_data = OAPTaggerDataset.splits(
        fields=fields, root=ROOT, train=TRAIN_FILE, validation=VAL_FILE, test=TEST_FILE)

    val_f = os.path.join(ROOT, OAPTaggerDataset.name, VAL_FILE)
    for ex, src_text in zip( val_data, examples_src_text(val_f) ):
        assert len(ex.text)==len(src_text)
        ex.src_text = src_text

    test_f = os.path.join(ROOT, OAPTaggerDataset.name, TEST_FILE)
    for ex, src_text in zip( test_data, examples_src_text(test_f) ):
        assert len(ex.text)==len(src_text)
        ex.src_text = src_text

    del test_data.fields['tags']
    # print(vars(train_data.examples[0]))
    # print(vars(test_data.examples[0]))
    tag_field.build_vocab(train_data)
    # print(tag_field.vocab.stoi)
    return train_data, val_data, test_data

def get_device():
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print('DEVICE==================== ',device)
    device = torch.device(device)
    return device

def get_iterators(train_data, val_data, test_data, device, batch_sz=32):
    dsets = (train_data, val_data, test_data)
    return data.BucketIterator.splits(datasets=dsets, batch_sizes=(batch_sz,1,1), device=device)

class OAPTaggerModel(nn.Module):
    def __init__(self, bert_model, hid_dim, out_dim):
        super().__init__()
        self.bert_model = bert_model
        emb_dim = bert_model.config.to_dict()['hidden_size']

        self.b_norm = nn.BatchNorm1d( emb_dim )

        self.fc1  = nn.Linear( emb_dim, hid_dim )
        self.relu = nn.ReLU()
        self.fc2  = nn.Linear( hid_dim, out_dim )

    def forward(self, tokens):
        tokens = tokens.permute( 1, 0 ) # [seq len, batch sz] -> [batch sz, seq len]

        emb = self.bert_model(tokens)[0] # -> [batch sz, seq len, emb dim]

        emb = emb.permute( 0, 2, 1 ) # ->[batch sz, emb dim, seq len]
        emb = self.b_norm( emb )
        emb = emb.permute( 2, 0, 1 ) # ->[seq len, batch sz, emb dim]

        emb = self.fc1 ( emb )
        emb = self.relu( emb ) # -> [seq len, batch sz, hid dim]
        return self.fc2( emb )

def train_epoch(model, iterator, optimizer, loss_fn):
    epoch_loss = 0
    model.train()
    for batch in tqdm(iterator):
        text = batch.text # [seq len, batch sz]
        tags = batch.tags # [seq len, batch sz]
        optimizer.zero_grad()
        preds = model(text) # [seq len, batch sz, out dim]
        preds = preds.view(-1, preds.shape[-1]) # [seq len * batch sz, out dim]
        tags = tags.view(-1) # [seq len * batch sz]
        loss = loss_fn(preds, tags)
        loss.backward()
        optimizer.step()
        epoch_loss += loss.item()
        # break
    return epoch_loss/len(iterator)

# def f1_score(preds, y, tag_pad_idx, device):
#     # , average=None
#     f1 = F1(num_classes=preds.shape[1], ignore_index=tag_pad_idx).to(device)
#     res = f1(preds, y)
#     return res

from f1_score_partial import f1_score

def eval_epoch(model, iterator, loss_fn, tag_field, device):
    epoch_loss = 0
    model.eval()
    y_true = []
    y_pred = []
    with torch.no_grad():
        for batch in iterator:
            preds = model(batch.text)
            tags  = batch.tags.view(-1)
            preds = preds.view(-1, preds.shape[-1])
            epoch_loss += loss_fn(preds, tags).item()

            true_tags = [tag_field.vocab.itos[tp.item()] for tp in tags]
            true_tags = true_tags[1:]
            y_true.append( true_tags )

            preds = preds.argmax(-1).tolist()
            pred_tags = [tag_field.vocab.itos[tp] for tp in preds]
            pred_tags = pred_tags[1:]
            y_pred.append( pred_tags )

            assert len(true_tags)==len(pred_tags)

    epoch_f1 = f1_score( y_true, y_pred )
    return epoch_loss/len(iterator), epoch_f1[0]

def train_model(model, train_iter, val_iter, optimizer, loss_fn, tag_field, device, n_epochs):
    best_val_loss = float('inf')
    for epoch in range(n_epochs):
        # print(f"{epoch=}")
        train_loss = train_epoch( model, train_iter, optimizer, loss_fn )
        val_loss, val_f1 = eval_epoch( model, val_iter, loss_fn, tag_field, device)
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            torch.save(model.state_dict(), BEST_MODEL_CHECKPOINT)
        print(f'Epoch: {epoch:02}, train loss: {train_loss:.3f}, val loss: {val_loss:.3f}, val f1: {val_f1:.2f}')

def get_text_tensor(text_tokens, text_field, device):
    text_tokens = [text_field.init_token] + text_tokens
    token_tensor = torch.LongTensor(text_tokens)
    return token_tensor.unsqueeze(-1).to(device)

def get_non_pad_elements(elems, tag_pad_idx):
    non_pad_elements = (elems != tag_pad_idx).nonzero()
    return elems[non_pad_elements].squeeze()

def predict(model, data, text_field, tag_field, device):
    model.eval()
    res = []
    print(f"Predicting ...")
    with torch.no_grad():
        for example in tqdm(data):
            text_tokens = get_text_tensor(example.text, text_field, device)
            pred_probas = model(text_tokens)
            pred_probas = pred_probas.view(-1, pred_probas.shape[-1])
            preds = pred_probas.argmax(-1)
            pred_tags = [tag_field.vocab.itos[tp.item()] for tp in preds]
            assert len(text_tokens) == len(pred_tags)
            pred_tags = pred_tags[1:]
            res.extend(pred_tags)
    return res

def generate_preds(model, data, text_field, tags_field, device, out_file):
    model.load_state_dict( torch.load(BEST_MODEL_CHECKPOINT) )
    preds = predict(model, data, text_field, tags_field, device)
    preds = iter(preds)
    with open(out_file, "w") as res:
        for example in data:
            text = example.src_text
            for word in text:
                pred = next(preds)
                res.write(f'{word}\t{pred}\n')
            res.write('\n')
        try:
            next_item = next(preds)
            assert False, f"unexpected item left in preds:{next_item}"
        except StopIteration:
            pass
    zip_file = out_file[:-3]+'zip'
    zipfile.ZipFile(zip_file, mode='w', compression=zipfile.ZIP_DEFLATED).write(out_file)
    print(f"Generated : '{out_file}', '{zip_file}'")

def main():
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    bert_model = BertModel.from_pretrained('bert-base-uncased')

    text_field, tag_field = get_fields(tokenizer)
    train_data, val_data, test_data = get_data(text_field, tag_field)

    device = get_device()
    model = OAPTaggerModel(bert_model, hid_dim=1024, out_dim=len(tag_field.vocab) ).to(device)

    optimizer = optim.AdamW( model.parameters(), lr=5e-5, weight_decay=0.1 )
    tag_pad_idx = tag_field.vocab.stoi[ tag_field.pad_token ]
    loss_fn = nn.CrossEntropyLoss( ignore_index=tag_pad_idx ).to(device)

    train_iter, val_iter, test_iter = \
        get_iterators(train_data, val_data, test_data, device)

    train_model(model, train_iter, val_iter, optimizer, loss_fn, tag_field, device, n_epochs=11 )

    PREDICT_ON = test_data
    generate_preds(model, PREDICT_ON, text_field, tag_field, device, out_file="output.tsv")

if __name__ == "__main__":
    main()